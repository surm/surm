%% PLAUSIBILITY_CHECKS
%  PLAUSIBILITY_CHECKS   Perform some plausibilty checks for SURM for
%                         1) Tmrt
%                         2) VF_j->i
%                         for cases for which the results are known. 
%
%  METHOD:
%   
%   Define test cases, calculate the results and assign results to variable test.
%   For a successful test, all entries in test should be true.
%
%  REFERENCES:
%
%   This program was used for  the plausibilty tests in
%
%    Section 3.2 Plausibility tests for surface-to-surface view factors and Tmrt
%
%    in
%
%    Fischereit, J., 2020:  The Simple Urban Radiation Model for 
%                  estimating Mean Radiant Temperature in idealized 
%                  street canyons, Urban Climate,
%                  https://doi.org/10.​1016/​j.​uclim.​2020.​100694
%
%% add surm model to path
addpath('../model')
clear all
%
%% set general parameter for surm
%------- street design ------
%
h        =  0;
w        =  100;
ori      =  57; 
%
%------- Built-up environment properties ------
%
T_w_init  = 293.15; % wall temperature
T_g_init  = 293.15; % ground temperature
epsilon_w = 0.9;    % emissivity of wall, from Dai 2012
epsilon_g = 0.95;   % emissivity of ground, from Dai 2012
alpha_w   = 0.15;   % mean wall albedo (for ENVI-met 3.1)
alpha_g   = 0.15;   % mean ground albedo (for ENVI-met 3.1)
%
%------- Metorological input --------
%
T_a2m   = 20 + 273.15; % air temperature in 2m [K]
RH_a2m  = 50;          % Relative humidity in 2m [%]
z_nn    = 11;          % Station height [m]; Fuhlsbuettel = 11 m
%
%------- Radiation input --------
%
phi     = 53;          % latitude
year    = 2015;        % ATTENTION: No leap years!
month   = 6;
day     = 21;
hour    = 12;
fileID  = fopen('../model/tlam2.txt');
tlam2_cell=textscan(fileID,'%4s %f','HeaderLines',7,'Delimiter',' '); 
%
%------ human characteristics input ------
%
alpha_k   = 0.7;  % absorption coefficient of the irradiated body surface 
                  % for short-wave radiation (default: 0.7)
epsilon_p = 0.97; % emissivity of the human body (default: 0.97)
pos_rel   = 0.5;  % position within the street canyon as seen from right to
                  % left in a N-S street canyon, valid are values 
                  % between 0 and 1
%
% conversions
% 1) julian day in radian
jd     = datenum(year,month,day) - datenum(year, 1, 1) + 1; 
%
% 2) RH in e using magnus equation
e_2mPa = ...
         (RH_a2m*6.112*exp(17.62*(T_a2m-273.15)/(243.12+(T_a2m-273.15))))...
         /100*100;
%
%% PLAUSIBILITY CHECK TMRT  
% Plausibility check (1): width and orientation should have no effect if h = 0
ori = [0:5:180];
w  = [5:400];
for iori = 1:length(ori)
    for iw = 1:length(w)
        tmrt(iori,iw) = calcTmrt(h,w(iw),ori(iori),...
                    epsilon_w,epsilon_g,alpha_w,alpha_g,...
                    T_a2m,e_2mPa,...
                    phi,jd,hour,...
                    alpha_k,epsilon_p,pos_rel,...
                    'T_w',T_w_init,'T_g',T_g_init,...
                    'TLam2',tlam2_cell{1,2}(month),'z_nn',z_nn,'BASIS_TS','diagnostic','v_10',2,'bo',0.5);
    end
end
test(1) = max(max(tmrt)) == min(min(tmrt));
% Plausibility check (2): for a person at the center of the street results are 
%                         symmetric
ori = [0:10:360];
w  = [20];
h  = 10;
hour = [0:23];
for iori = 1:length(ori)
    for ihour = 1:length(hour)
        tmrt1(iori,ihour) = calcTmrt(h,w(1),ori(iori),...
                    epsilon_w,epsilon_g,alpha_w,alpha_g,...
                    T_a2m,e_2mPa,...
                    phi,jd,hour(ihour),...
                    alpha_k,epsilon_p,pos_rel,...
                    'T_w',T_w_init,'T_g',T_g_init,...
                    'TLam2',tlam2_cell{1,2}(month),'z_nn',z_nn,'BASIS_TS','diagnostic','v_10',2,'bo',0.5);
    end
end
%% checks
for i = 1:(length(ori)-1)/2
    diff(i,:)=tmrt1(i,:)-tmrt1((length(ori)-1)/2+i,:);
end
test(2) = all(all(diff<10^-8));
%
%% PLAUSIBILITY CHECK VF_j->i
% case (1)
h = 10;
w = 5;
frac_floor=0;
frac_wall =0;
[vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe] = ...
    se_calc_vf_a2a(h, w, frac_floor, frac_wall);
h1=h/w;
vfg2w = 1/2*(1+h1-sqrt(1+h1^2));
test(3)= vfg2w == vf_shg.shw & vfg2w == vf_shg.shwe;
% case (2)
h2=w/h;
vfw2w = sqrt(1+h2^2)-h2;
test(4)= vfw2w == vf_shw.shwe & vfw2w == vf_shwe.shw;
%
% case (3)
frac_wall =1;
[vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe] = ...
    se_calc_vf_a2a(h, w, frac_floor, frac_wall);
test(5)=vfg2w == vf_shg.ilw & vfg2w == vf_shg.shwe;
% case (4)
test(6) = vfw2w == vf_ilw.shwe & vf_shwe.ilw;
%
%% RESULTS
if (all(test))
   disp('SUCCESS! All tests successful.')
else
   disp('ATTENTION! Some test have failed.')
   test
end
