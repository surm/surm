function [R_sw, I_0] = se_calc_sw_ground(...
    I_infty, alpha_sun, jd, pb, p0, TLam2,basis_dir_rad)

% SE_CALC_SW_GROUND     Calculates SW-radiation which reaches the
%                       ground (reduces solar constant by absorption in
%                       atmosphere) for a surface normal to the incoming
%                       radiation
%
%  INPUT:
%
%    I_infty             -   Solar constant
%    alpha_sun           -   Elevation angle of the sun [rad]
%    mu_0                -   Optical thickness of the atmosphere
%    jd                  -   Julian Day
%    pb                  -   Pressure in station height
%    p0                  -   Mean sea level pressure
%    TLam2               -   Linke turbidity factor of relative optical
%                            air mass 2
%    basis_dir_rad       -   Define basis of short wave calculation
%                            applied here (valid are: 'MITRAS',
%                            'Bruse1999', 'vdi1994','esra')
%
%
%  OUTPUT:
%
%    R_sw                -   SW direct radiation reaching ground
%    I_0                 -   solar constant with accounted annual cycle
%
%  REFERENCES
%
%    Gierisch, A., 2011: Mikroskalige Modellierung meteorologischer und
%       anthropogener Einflüsse auf die Wä̈rmeabgabe eines Gebäudes
%       Master’s thesis, Geowissenschaften, University of Hamburg,
%       Hamburg, Germany, p. 12
%
%    Rigollier C, Bauer O, Wald L (2000): On the clear sky model of the
%       ESRA - European Solar Radiation Atlas - with respect to the
%       HELIOSAT method. Solar Energy, 68 (1): 33 - 48
%
%    VDI 3789, 2 (1994): Interactions between Atmosphere and Surfaces
%      Calculation of Short-wave and Long-wave Radiation
%
%% DEFINITION OF D2R
%  because: matlab intrinsic function deg2rad is slow.
d2r = (pi()/180);
r2d = (180/pi());

%% ANNUAL CYCLE OF SOLAR "CONSTANT"
% Based on VDI 3789, 2 (1994), p. 16
I_0 = I_infty*(1+0.03344*cos(d2r*(0.9856*jd-2.72)));
%
%% PARAMETERIZE EFFECT OF ABSORPTION OF SW RADIATION WITHIN ATMOSPHERE
if alpha_sun < 0 
    R_sw = 0;
else
    switch basis_dir_rad
        case lower('MITRAS')
            % radiation lost in atmosphere due to optical depth of atmosphere
            mu_0  = 0.75; % valid for Northern Germany
            gamma = 1./(1+8*(sin(alpha_sun)).^0.7);
            R_sw = I_0 * mu_0/(1+ gamma/(1-gamma));
        case lower('vdi1994')
            % VDI 3789,2, p.33
            % relative optical air mass
            mr0 = 1/(sin(alpha_sun) + 0.50572* ...
                (r2d*alpha_sun + 6.07995)^(-1.6364));
            mr = mr0 * (pb/p0);
            % Rayleigh optical thickness
            if alpha_sun*r2d > 5
                deltar0 = 1/(9.4 + 0.9 * mr0);
            else
                % number in brackets = alpha_sun*r2d-1
                deltar0_tmp(6) = 0.0548;
                deltar0_tmp(5) = 0.0519;
                deltar0_tmp(4) = 0.0491;
                deltar0_tmp(3) = 0.0463;
                deltar0_tmp(2) = 0.0435;
                deltar0_tmp(1) = 0.0408;
                %
                deltar0 = deltar0_tmp(floor(alpha_sun*r2d+1)) ...
                    + (deltar0_tmp(ceil(alpha_sun*r2d+1)) ...
                    -deltar0_tmp(floor(alpha_sun*r2d+1)))...
                    * (alpha_sun*r2d - floor(alpha_sun*r2d));
            end
            R_sw = I_0 * exp(-TLam2*deltar0*mr);
        case lower('esra')
            % relative optical air mass
            mr0 = 1/(sin(alpha_sun) + 0.50572* ...
                (r2d*alpha_sun + 6.07995)^(-1.6364));
            mr = mr0 * (pb/p0);
            % Rayleigh optical thickness
            if mr > 20
                deltar0 = 1/(10.4+0.718*mr);
            else
                deltar0 = 1/...
                    (6.625928 + 1.92969 * mr0 - 0.170073 * mr0^2 + 0.011517 * mr0^3 -0.000285 * mr0^4);
            end
            % dependence on station height (important in mountainous
            % regions)
            pc750 = 1.248274 - 0.011997 * deltar0 + 0.00037 * deltar0^2;
            pc500 = 1.68219  - 0.03059  * deltar0 + 0.00089 * deltar0^2;
            if pb > 750
                pc = 1 + (pc750 - 1) * (1 - pb/p0);
            elseif pb <= 750 && pb > 500
                pc = pc750 + (pc500 - pc750) * (0.75 - pb/p0) /0.25;
            elseif pb <=500
                pc = pc500;
            end
            deltar = deltar0 / pc;
            R_sw = I_0 * ...
                   exp(-0.8662 * TLam2 * mr * deltar);
        otherwise
            error('Unknown value for basis_sw_absorp')
    end
end
end
