function[vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe] = ...
    se_calc_vf_a2a(h, w, frac_floor, frac_wall)

    % SE_CALC_VF_A2A    Calculates all necessary view factors between two
    %                     surfaces
    %
    %  INPUT:
    %
    %    h          -   Height of building
    %    w          -   Distance to building wall
    %    frac_floor -   fraction of floor that is illuminated
    %    frac_wall  -   fraction of wall that is illuminated
    %
    %  OUTPUT:
    %
    %    vf_shg     -   View factors of shaded ground (svf, wvf)
    %    vf_ilg     -   View factors of illuminated ground (svf, wvf)
    %    vf_ilw     -   View factors of illuminated wall (svf, gvf)
    %    vf_shw     -   View factors of shaded wall (svf, gvf)
    %    vf_shwe    -   View factors of entirely shaded wall (svf, gvf)
    %
    %% default: no walls
    vf_ilw.shg   = 0;
    vf_ilw.ilg   = 0;
    vf_ilw.s     = 0;
    vf_ilw.shwe  = 0;
    vf_shw.shg   = 0;
    vf_shw.ilg   = 0;
    vf_shw.s     = 0;
    vf_shw.shwe  = 0;
    vf_shwe.ilw  = 0;
    vf_shwe.shw  = 0;
    vf_shwe.shg  = 0;
    vf_shwe.ilg  = 0;
    vf_shwe.s    = 0;
    %
    vf_shg.shwe  = 0;
    vf_shg.shw   = 0;
    vf_shg.ilw   = 0;
    vf_ilg.shwe  = 0;
    vf_ilg.shw   = 0;
    vf_ilg.ilw   = 0;
    if (frac_floor == 0 && frac_wall == 0) % night
        vf_ilg.s     = 0;
        vf_shg.s     = 1;
    else
        vf_ilg.s     = 1;
        vf_shg.s     = 0;
    end
    if h == 0
        return
    end
    %% Only if walls exist
    if frac_floor < 1 % ground shaded by walls or night
        vf_shg.shwe  = se_vfp2pwoceAngle(0,(1-frac_floor)*w,0,h,pi/2);
        vf_shg.shw   = se_vfp2pwoceAngle(frac_floor*w,w,0,(1-frac_wall)*h,pi/2);
        vf_shg.ilw   = se_vfp2pwoceAngle(frac_floor*w,w,(1-frac_wall)*h,h,pi/2);
        vf_shg.s     = 1 - vf_shg.ilw - vf_shg.shw - vf_shg.shwe;
        %
        vf_shwe.ilg  = se_vfp2pwoceAngle(0,h,(1-frac_floor)*w,w,pi/2);
        vf_shwe.shg  = se_vfp2pwoceAngle(0,h,0,(1-frac_floor)*w,pi/2);
        vf_shwe.s    = se_vfp2pwoceAngle(0,h,0,w,pi/2);
        vf_shwe.ilw  = se_vfp2pp(h,sqrt(w^2+(frac_wall*h)^2),sqrt(h^2+w^2),...
                sqrt(w^2+((1-frac_wall)*h)^2),w);
        vf_shwe.shw  = 1 - vf_shwe.ilg - vf_shwe.shg - vf_shwe.s - vf_shwe.ilw;
        if (vf_shwe.shw < 10^-8)
           vf_shwe.shw  = 0;
        end
        %
        if frac_floor == 0 && frac_wall < 1 % ground fully shaded by building or night
            vf_shw.shg   = se_vfp2pwoceAngle(0,(1-frac_wall)*h,0,w,pi/2);
            vf_shw.s     = se_vfp2pwoceAngle(frac_wall*h,h,0,w,pi/2);
            vf_shw.ilg   = 0;
            vf_shw.shwe  = 1 - vf_shw.ilg - vf_shw.shg - vf_shw.s;
        end
        %
        if frac_wall > 0 % day (partly shaded wall and frac_floor may be > 0)
            vf_ilw.ilg   = se_vfp2pwoceAngle((1-frac_wall)*h,h,0,(frac_floor)*w,pi/2);
            vf_ilw.shg   = se_vfp2pwoceAngle((1-frac_wall)*h,h,(frac_floor)*w,w,pi/2);
            vf_ilw.s     = se_vfp2pwoceAngle(0,frac_wall*h,0,w,pi/2);
            vf_ilw.shwe  = 1 - vf_ilw.ilg - vf_ilw.shg - vf_ilw.s;            
        end
        %
        if frac_floor > 0 % partly shaded ground
            vf_ilg.shwe  = se_vfp2pwoceAngle((1-frac_floor)*w,w,0,h,pi/2);
            vf_ilg.ilw   = se_vfp2pwoceAngle(0,frac_floor*w,0,frac_wall*h,pi/2);
            vf_ilg.shw   = 0; 
            vf_ilg.s     = 1 - vf_ilg.ilw - vf_ilg.shw - vf_ilg.shwe;
        end
    elseif frac_floor == 1  % sun parallel to canyon
        vf_ilg.shwe  = se_vfp2pwoceAngle((1-frac_floor)*w,w,0,h,pi/2);
        vf_ilg.shw   = vf_ilg.shwe;
        vf_ilg.ilw   = 0;
        vf_shw.ilg   = se_vfp2pwoceAngle(0,h,0,w,pi/2);
        vf_shw.s     = vf_shw.ilg;
        vf_shw.shwe  = 1 - vf_shw.ilg - vf_shw.s;
        vf_shwe.ilg  = vf_shw.ilg;
        vf_shwe.s    = vf_shw.ilg;
        vf_shwe.shw  = vf_shw.shwe;
        vf_shwe.ilw  = 0;
        % no wall lit
        vf_ilw.ilg   = 0;
        vf_ilw.shg   = 0;
        vf_ilw.shwe  = 0;
        vf_ilw.s     = 0;
    end
    %    
end
