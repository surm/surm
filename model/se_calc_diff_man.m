function[D_t]=se_calc_diff_man(D_s,R_sw,alpha_w,alpha_g,vfp2,...
        vf_shag, vf_litg, vf_litw, vf_shaw, vf_shawe,cos_eta)

    % SE_CALC_DIFF_MAN       Calculates total diffuse radiation
    %
    %  INPUT:
    % 
    %    D_s             -   Diffuse SW radiation from sky (actually only
    %                        isotropic component) 
    %    R_sw            -   SW dirct radiation after absorption in atm +
    %                        corrected for sun height 
    %    alpha_w         -   Mean albedo of walls
    %    alpha_g         -   Mean albedo of ground
    %    vfp2            -   view factor of person to surfaces
    %    vf_shag         -   View Factors for shaded ground to other
    %                        surfaces
    %    vf_litg         -   View Factors for lit ground to other surfaces
    %    vf_litw         -   View Factors for lit wall to other surfaces
    %    vf_shaw         -   View Factors for shaded wall to other surfaces
    %    vf_shawe        -   View Factors for entirley shaded wall to other
    %                        surfaces
    %    cos_eta         -   cos(eta) angle for inclined surfaces
    %   
    %
    %  OUTPUT:
    %
    %    D_t            -   total diffuse Radiation reaching the human body
    %
    %
    %% CALCUALTION
    %
    if isnan(cos_eta(1))
        sw_dir_refl_w = 0; % sun parallel to canyon
    else
        sw_dir_refl_w = alpha_w * R_sw * cos_eta(1);        
    end
    D_t = vfp2.w.lit               * sw_dir_refl_w ...
        + vfp2.w.sha * vf_litw.s   * alpha_w * D_s ...
        + vfp2.w.sha * vf_shaw.s   * alpha_w * D_s ...
        + vfp2.w.shae* vf_shawe.s  * alpha_w * D_s ...
        + vfp2.g.lit               * alpha_g * R_sw * cos_eta(2) ...
        + vfp2.g.sha * vf_shag.s   * alpha_g * D_s ......
        + vfp2.g.lit * vf_litg.s   * alpha_g * D_s ...
        + vfp2.s                             * D_s;
    %
    %
end
