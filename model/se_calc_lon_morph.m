function[E_g, E_w, T_g, T_w]=se_calc_lon_morph(basis_ts,T_gi,T_wi,T_a2m,epsilon_g,...
    epsilon_w,sigma,frac_wall,frac_floor,E_s,D_s,alpha_g,R_sw,...
    cos_eta,ill_wall,alpha_w,v_10,bo_g, h,...
    vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe)

    % SE_CALC_LON_MORPH       Calculates longwave radiation from ground and
    %                             walls
    %
    %  INPUT:
    % 
    %    basis_ts        -   Handling of surface temperatures of ground and
    %                        walls ('prescribed' (prescribed values for
    %                        sun-lit wall and ground during the day and
    %                        entire surfaces during the night) and 
    %                        'diagnostic' (diagnostic calculation of ts)).
    %    T_gi            -   Temperature of ground (mean, initial, [K])
    %    T_wi            -   Temperature of wall (mean, initial, [K])
    %    T_a2m           -   Temperature in 2m [K]  
    %    epsilon_g       -   Emissivity of the ground
    %    epsilon_w       -   Emissivity of the wall
    %    sigma           -   Stefan-Boltzman-Konstant [Wm^-2K^-4]
    %    frac_wall       -   fraction of wall that is illuminated
    %    frac_floor      -   fraction of floor that is illuminated
    %    E_s             -   Longwave radiation from sky [Wm^-2]
    %    D_s             -   Diffuse radiation from sky [Wm^-2]
    %    alpha_g         -   Mean albedo of ground
    %    R_sw            -   SW from sky reaching ground without consideration
    %                        of city's morphology for different times [Wm^-2]
    %    cos_eta         -   Azimuth angle of (1) wall, (2) ground
    %    ill_wall        -   Illuminated wall ('l' left, 'r' right, 'b'
    %                        both, with respect to N-S canyon)
    %    alpha_w         -   Mean albedo of walls
    %    v_10            -   wind speed in 10 m, only used if
    %                        l_e_diagnostic, to estimate wall and ground
    %                        temperature [m/s]
    %    bo_g            -   Bowen ratio: 0.6 grass, 1.3 urban
    %    h               -   Building height [m]
    %    vf_shg          -   View factors from shaded ground to other surfaces
    %    vf_ilg          -   View factors from lit    ground to other surfaces 
    %    vf_ilw          -   View factors from lit    wall   to other surfaces
    %    vf_shw          -   View factors from shaded wall   to other surfaces
    %    vf_shwe         -   View factors from entirely shaded wall to other surfaces
    %   
    %
    %  OUTPUT:
    %
    %    E_g    -   Longwave radiation from ground (1: lit, 2: shaded, [Wm^-2])
    %    E_w    -   Longwave radiation from walls (1: lit, 2: shaded, 3: shaded_entirely, [Wm^-2])
    %    T_g    -   Ground temperature  (1: lit, 2: shaded, [Wm^-2])
    %    T_w    -   Wall temperature (1: lit, 2: shaded, 3: shaded_entirely, [Wm^-2])
    %
    %%
    switch basis_ts
        case 'prescribed'
            if R_sw > 0 % during the day
                T_g_sha  = T_a2m;
                T_w(2)   = T_a2m;
                T_w(3)   = T_a2m;
            else
                T_w(2)   = T_wi;
                T_w(3)   = T_wi;
                T_g_sha  = T_gi;
            end
            T_w(1)   = T_wi;
            T_g_ill  = T_gi;
            E_g(1)   = epsilon_g * sigma * T_g_ill^4;
            E_g(2)   = epsilon_g * sigma * T_g_sha^4;                        
            E_w(1)   = epsilon_w * sigma * T_w(1)^4;
            E_w(2)   = epsilon_w * sigma * T_w(2)^4; 
            E_w(3)   = epsilon_w * sigma * T_w(3)^4; 
        case 'diagnostic'
            %
            % iterative process of wall and ground together use Newton
            % Approximation --> instantanous equilibrium, heat storage
            %
            % initial values for wall temperature, since needed for first
            % ground calculation
            T_w(1)  = T_a2m;
            T_w(2)  = T_a2m;
            T_w(3)  = T_a2m;                       
            E_w(1)  = epsilon_w * sigma * T_w(1)^4;                       
            E_w(2)  = epsilon_w * sigma * T_w(2)^4;                     
            E_w(3)  = epsilon_w * sigma * T_w(3)^4;
            %
            %
            % SHORTWAVE BALANCE -------------------------------------------
            %   default values (night)
            E_swnetg_ill  = 0;
            E_swnetg_sha  = 0;
            E_swnetw(1:3) = 0;
            %
            %            
            if R_sw > 0 % day
                % net shortwave balance at 
                %
                % ground: 
                E_swnetg_ill = (1-alpha_g) * (R_sw * cos_eta(2) + ...
                                 vf_ilg.s   * D_s + ...
                                 vf_ilg.ilw * alpha_w * R_sw * cos_eta(1) + ...
                                 vf_ilg.ilw * alpha_w * D_s + ...
                                 vf_ilg.shwe* alpha_w * D_s + ...
                                 vf_ilg.shw * alpha_w * D_s   ...
                                );
                E_swnetg_sha = (1-alpha_g)  * (...
                                 vf_shg.s   * D_s + ...
                                 vf_shg.ilw * alpha_w * R_sw * cos_eta(1)+ ...
                                 vf_shg.ilw * alpha_w * D_s + ...
                                 vf_shg.shwe* alpha_w * D_s + ...
                                 vf_shg.shw * alpha_w * D_s   ......
                                );
                % walls: different wall cases:
                %  1: illuminated part
                %  2: shaded part
                %  3: shaded wall
                if h > 0  
                    if strcmp(ill_wall,'n')
                        E_swnetw(1) = 0;
                    else
                        E_swnetw(1) = (1-alpha_w)*(cos_eta(1)*R_sw...
                                     + vf_ilw.s   * D_s + ...
                                     + vf_ilw.ilg * alpha_g * cos_eta(2) * R_sw + ...
                                     + vf_ilw.ilg * alpha_g * D_s + ...
                                     + vf_ilw.shg * alpha_g * D_s + ...
                                     + vf_ilw.shwe* alpha_w * D_s);
                    end
                    E_swnetw(2) = (1-alpha_w) * ( ...
                                   vf_shw.s   * D_s + ...
                                 + vf_shw.ilg * alpha_g * cos_eta(2) * R_sw + ...
                                 + vf_shw.ilg * alpha_g * D_s + ...
                                 + vf_shw.shg * alpha_g * D_s + ...
                                 + vf_shw.shwe* alpha_w * D_s);
                    E_swnetw(3)= (1-alpha_w)*( ...
                                   vf_shwe.s   * D_s + ...
                                 + vf_shwe.ilg * alpha_g * cos_eta(2) * R_sw + ...
                                 + vf_shwe.ilg * alpha_g * D_s + ...
                                 + vf_shwe.shg * alpha_g * D_s + ...
                                 + vf_shwe.shw * alpha_w * D_s + ...
                                 + vf_shwe.ilw * alpha_w * D_s+ ...
                                 + vf_shwe.ilw * alpha_w * cos_eta(1) * R_sw);
                end
            end
            %
            % LONGWAVE BALANCE --------------------------------------------
            for j = 1 : 5 % iteration for balance between ground and wall temperature
                %
                % Ground 
                %  1: illuminated part
                %  2: shaded part
                E_lwnetg_sha =  vf_shg.s   * E_s ...
                              + vf_shg.ilw * E_w(1) ...
                              + vf_shg.shw * E_w(2) ...
                              + vf_shg.shwe* E_w(3);
                T_g_sha = se_calc_Ts(E_swnetg_sha, epsilon_g, ...
                          E_lwnetg_sha, sigma, T_a2m, bo_g, v_10, 'g');
                T_g_ill = T_g_sha; % default: illuminated and shaded part same T
                if frac_floor > 0
                    E_lwnetg_ill =  vf_ilg.s   * E_s ...
                                  + vf_ilg.ilw * E_w(1) ...
                                  + vf_ilg.shw * E_w(2)...
                                  + vf_ilg.shwe* E_w(3);
                    T_g_ill = se_calc_Ts(E_swnetg_ill, epsilon_g, ...
                              E_lwnetg_ill, sigma, T_a2m, bo_g, v_10, 'g'); 
                end
                %
                E_g(1)  = epsilon_g * sigma * T_g_ill^4;
                E_g(2)  = epsilon_g * sigma * T_g_sha^4; 
                %
                % Walls
                if h > 0 % only if walls exist this calculation must be done
                    %
                    %
                    % Define different wall cases:
                    %  1: illuminated part
                    %  2: shaded part
                    %  3: shaded wall
                    E_lwinw(1) =  vf_ilw.s    * E_s ...
                                + vf_ilw.ilg  * E_g(1) ...
                                + vf_ilw.shg  * E_g(2) ...
                                + vf_ilw.shwe * epsilon_w * sigma * T_w(3)^4;
                    E_lwinw(2) =  vf_shw.s    * E_s ...
                                + vf_shw.ilg  * E_g(1) ...
                                + vf_shw.shg  * E_g(2) ...
                                + vf_shw.shwe * epsilon_w * sigma * T_w(3)^4;
                    E_lwinw(3) =  vf_shwe.s   * E_s ...
                                + vf_shwe.ilg * E_g(1) ...
                                + vf_shwe.shg * E_g(2) ...
                                + vf_shwe.ilw * epsilon_w * sigma * T_w(1)^4 ...
                                + vf_shwe.shw * epsilon_w * sigma * T_w(2)^4;
                    for i_wall = 3:-1:1
                        if i_wall == 1 && frac_wall == 0
                            % default: same temeprature as shaded wall
                            T_w(i_wall) = T_w(2);
                        else
                            T_w(i_wall) = se_calc_Ts(E_swnetw(i_wall), epsilon_w, ...
                                E_lwinw(i_wall), sigma, T_a2m, nan, v_10, 'w');
                        end
                        E_w(i_wall) = epsilon_w*sigma*T_w(i_wall)^4;
                    end
                end
            end
        % adjust result if parallel to canyon    
        if strcmp(ill_wall,'n')
            T_w(1) = nan;
            E_w(1) = 0;
        end
    end
    %
    if h <= 0 % no walls
        T_w(1:4) = nan;
        E_w(1) = 0;
        E_w(2) = 0;
        E_w(3) = 0;
    end
    T_w(4) = frac_wall*T_w(1) + (1-frac_wall)*T_w(2);
    %
    % Not needed for calculation of tmrt, only additional output
    % T_g(1) = T_lit or T_sha depending on any part of floor receives sun
    if frac_floor > 0
        T_g(1) = T_g_ill;
    else
        T_g(1) = T_g_sha;
    end
    % T_g(2) = T_sha always
    T_g(2) = T_g_sha;
    % T_g(3) = average ground temperature
    T_g(3) = (1-frac_floor) * T_g_sha + frac_floor * T_g_ill;
end
