function[fvi]=se_calc_vfperson(a,b,c,orientSurf,type,deltainner,deltaouter)
    
    % SE_CALC_VFPERSON    Calculates view factor of walls and ground to person 
    %
    %  INPUT:
    %
    %    a                  -   x-component of surface
    %    b                  -   z- or y-component of surface (depends on orientSurf)
    %    c                  -   distance to surface
    %    orientSurf         -   orientation of the surface, 
    %                           'hor' = horizontal (ground, sky), 
    %                           'ver' = vertical (walls)
    %    type               -   Calcualtion method ('Fanger', 'Fangerfast',
    %                            'Cannistraro')
    %    deltainner         -   maximal grid spacing in one direction (depends on
    %                            orientSurf)
    %    deltaouter         -   maximal grid spacing in other direction (depends on
    %                            orientSurf)
    %   
    %
    %  OUTPUT:
    %   
    %    fvi        -   view factor
    %
    %  REFERENCES
    %
    %    Cannistraro, G., Franzitta, G., Giaconia, C., & Rizzo, G. (1992). 
    %         Algorithms for the calculation of the view factors between 
    %         human body and rectangular surfaces in parallelepiped 
    %         environments. ENERGY AND BUILDINGS, 19, 51-60.
    %
    %    Fanger, P. O., 1970: Thermal Comfort - Analysis and Applications 
    %        in Environmental Engineering - Danisch Technical Press. pp 164
    %
    %    Vorre, M. H., R. L. Jensen, J. L. Dreau, 2015: Radiation exchange 
    %        between persons and surfaces for building energy simulations. 
    %        – Energy and Buildings 101, 110 – 121 
    %
    %%
    switch type
        case {lower('Fanger'),lower('Fangerfast')}
            maxouter   = a/c;
            maxinner   = b/c;
            % restrict grid spacing
            if a/c < deltaouter
                roundto    = 10^(-floor(log10(a/c)));
                deltaouter = floor(a/c*roundto)/roundto;
            end
            if b/c < deltainner
                roundto    = 10^(-floor(log10(b/c)));
                deltainner = floor(b/c*roundto)/roundto;
            end
            %
            if strcmp(type,lower('Fanger'))
                fvi=0;
                for outer = 0:deltaouter:maxouter
                    for inner = 0:deltainner:maxinner
                        integrand = se_fp2a(outer,inner,orientSurf);
                        fvi       = fvi + 1/pi * integrand * deltainner * deltaouter;
                    end
                end
            else
                NX         = 2*ceil(floor(maxouter/deltaouter)/2);
                NY         = 2*ceil(floor(maxinner/deltainner)/2);
                weight_mat              = ones(NX+1,NY+1);
                weight_mat(2:end-1,:)   = weight_mat(2:end-1,:)*2;
                weight_mat(:,2:end-1)   = weight_mat(:,2:end-1)*2;
                weight_mat(2:2:end-1,:) = weight_mat(2:2:end-1,:)*2;
                weight_mat(:,2:2:end-1) = weight_mat(:,2:2:end-1)*2;
                gridx = maxouter/NX;
                gridy = maxinner/NY;
                outer = 0:gridx:maxouter;
                inner = 0:gridy:maxinner;
                [horddisg, verddisg] = ndgrid(outer,inner);
                func    = @(hordis,verdis) se_fp2a(horddisg,verddisg,orientSurf);                
                act_mat = feval(func,horddisg,verddisg);
                fvi     = sum(sum(1/pi * act_mat .* weight_mat * 1/9 * gridx * gridy));  
            end
        case lower('Cannistraro')
            if strcmp(orientSurf,'ver')
             a1   = 1.24186;
             b1   = 0.16730;
             fmax = 0.120;
             c1   = 0.61648; 
             d    = 0.08165;
             e    = 0.05128;         
            elseif strcmp(orientSurf,'hor')
             a1   = 1.59512;
             b1   = 0.12788;
             fmax = 0.116;
             c1   = 1.22643; 
             d    = 0.04621;
             e    = 0.04434; 
            end
            tau   = a1 + b1 *(a/c);
            gamma = c1 + d *(b/c) + e*(a/c);
            fvi   = fmax * (1-exp(-(a/c)/tau)) * (1-exp(-(b/c)/gamma));
    end
end
%
%%
function[integrand]=se_fp2a(outer,inner,orientSurf)
    %
    % SE_FP2A    Calculates view factor of person to area 
    %
    %  INPUT:
    %
    %    outer            -   outer numerical grid spacing
    %    inner            -   inner numerical grid spacing
    %    orientSurf         -   orientation of the surface, 
    %                           'hor' = horizontal (ground, sky), 
    %                           'ver' = vertical (walls)
    %   
    %
    %  OUTPUT:
    %   
    %    integrand          -   integrand for the view factor integral
    %
    %
    %  REFERENCES
    %
    %    Fanger, P. O., 1970: Thermal Comfort - Analysis and Applications 
    %        in Environmental Engineering - Danisch Technical Press. pp 164 
    %%
    if strcmp(orientSurf,'ver')
        gamma = atan(inner./(sqrt(outer.^2+1)));
    elseif strcmp(orientSurf,'hor')
        gamma = atan(1./(sqrt(outer.^2+inner.^2)));
    end
    fp = se_calc_fp(gamma);
    integrand = fp./((1+outer.^2+inner.^2).^1.5);
end
