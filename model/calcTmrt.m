function[tmrt, varargout]=calcTmrt(h, w, ori,...
            epsilon_w, epsilon_g, alpha_w, alpha_g, ...
            T_a2m, e_2mPa,...
            phi, jd, hh, ...
            alpha_k, epsilon_p, pos_rel, ...
            varargin)
        
    % CALCTMRT    Calculates Tmrt for an idealized symmetrical street
    %             canyon with radiation parametersiations for cloudless
    %             conditions
    %
    %  REQUIRED INPUTS (in this order):
    %
    %    h               -   Building height [m]
    %    w               -   Building width [m]
    %    ori             -   Orientation angle of the street with respect to north [°]
    %    epsilon_w       -   Emissivity of the wall    
    %    epsilon_g       -   Emissivity of the ground
    %    alpha_w         -   Mean albedo of walls
    %    alpha_g         -   Mean albedo of ground
    %
    %    T_a2m           -   Temperature in 2m [K]
    %    e_2mPa          -   Water vapour pressure in 2m [Pa] 
    %
    %    phi             -   Geographical latitude [°]
    %    jd              -   Julian day number
    %    hh              -   hour of interest    
    %
    %    alpha_k         -   short wave absorption coefficient of the human body [default: 0.7]
    %    epsilon_p       -   long wave absorption coefficient (=Emissivity) of human body [default: 0.97]
    %    pos_rel         -   Position of person (between 0 and 1 as seen 
    %                        from right wall looking into the direction of ori)
    %
    %  OPTIONAL INPUTS (Needed only for specific parameterizations, input as
    %                   parameter-value-pairs)
    %
    %    T_w             -   Temperature of sunny wall (mean) [K]
    %    T_g             -   Temperature of sunny ground (mean) [K]
    %    v_10            -   wind speed in 10 m, only used if
    %                        basis_ts, to estimate wall and ground
    %                        temperature [m/s]
    %    TLam2           -   Linke turbidity factor of relative optical
    %                        air mass 2 can be taken from 
    %                        http://www.soda-pro.com/web-services/atmosphere/linke-turbidity-factor-ozone-water-vapor-and-angstroembeta
    %    bo              -   Bowen ratio: 0.5 grass, 1.3 urban
    %    z_nn            -   Station height above sea level [m]
    % 
    %
    % Optional arguments in this order:
    %
    %    basis_declin_calc-  Type of declination calculation (valid are: 
    %                        'MITRAS','vdi3789')
    %    basis_dir_rad  -    Define basis of short wave absorption
    %                        applied here (valid are: 'MITRAS',
    %                        'vdi1994','esra')
    %    basis_diff_rad  -   Define basis of diffuse SW parameterization
    %                        applied here (valid are: 'MITRAS',
    %                        'vdi1994','esra')
    %    basis_lw_sky    -   Define basis of long wave from sky
    %                        (valid are: 'MITRAS', 'Idso1981', 'vdi3789')
    %    basis_ts        -   Handling of surface temperatures of ground and
    %                        walls ('prescribed' (prescribed values for
    %                        sun-lit wall and ground during the day and
    %                        entire surfaces during the night) and 
    %                        'diagnostic' (diagnostic calculation of ts)).
    %    basis_pvf       -   Define basis of view factor calculation for person
    %                        (valid are: 'Fanger',
    %                        'Fangerfast','Cannistraro')
    %    l_dispValues    -   Logical: Assign calculated values of different
    %                        terms to argout (true,false)
    %   
    %
    %  OUTPUT:          Optional outputs are returned, if l_dispValues=true
    %   
    %    tmrt                  - Mean radiant temperature at chosen point
    %                             within street canyon in [K]
    %    is_pos_lit [optional] - Is position of person lit
    %    lw         [optional] - longwave radiation component [W/m2]
    %    dif        [optional] - diffusive radiation component [W/m2]
    %    dis        [optional] - direct radiation component [W/m2]
    %    ges        [optional] - total radiation [W/m2]
    %    T_g        [optional] - Ground Temperature [K]
    %    T_w        [optional] - Wall Temperature [K]
    %    Glob       [optional] - Global radiation [W/m2]
    %    D_s        [optional] - Diffuse radiation from sky [W/m2]
    %    E_s        [optional] - Longwave radiation from sky [W/m2]
    %    R_sw       [optional] - Direct radiation  [W/m2]
    %    D_reflg    [optional] - Reflected SW radiation at ground [W/m2]
    %    E_g        [optional] - Longwave radiation from ground [W/m2]
    %    ill_wall   [optional] - Illumunated wall ['l','r','n']
    %    f_p        [optional] - Projection factor
    %    cos_eta    [optional] - Cos(eta) angle for inclined surfaces
    %    alpha_sun  [optional] - Elevation angle of the sun [rad]
    %    vfp2       [optional] - View factors of person to surrounding surfaces
    %    vf_shg     [optional] - View factor of shaded ground to other surfaces
    %    vf_ilg     [optional] - View factor of lit ground to other surfaces
    %    vf_ilw     [optional] - View factor of lit wall to other surfaces
    %    vf_shwe    [optional] - View factor of entirely shaded wall to other surfaces
    %
    %  BACKGROUND:
    %
    %    Tmrt is defined as the uniform temperature of a fictive black-body 
    %    radiation enclosure in which a subject would experience the same 
    %    net-radiation energy exchange as in the actual more complex 
    %    radiation environment (according to Kantor, N., J. Unger, 2011)
    %
    %  REFERENCES:
    %
    %    Fischereit, J., 2020:  The Simple Urban Radiation Model for 
    %                  estimating Mean Radiant Temperature in idealized 
    %                  street canyons, Urban Climate, 
    %                  https://doi.org/10.​1016/​j.​uclim.​2020.​100694
    %    
    %% DEFAULT OPTIONS AND OPTIONAL INPUT ARGUMENTS
    [T_w, T_g, v_10, TLam2, bo, z_nn, ...
    basis_declin_calc, basis_dir_rad, basis_diff_rad, ...
    basis_lw_sky, basis_ts, basis_pvf, l_dispValues] = se_parse_Input2(varargin);
        
    %% CONSTANTS
    p0      = 1013.25;                 % pressure at NN [hPa]
    sigma   = 5.67*10^(-8);            % Boltzmann
    pb      = p0 * exp(-z_nn/8434.5);  % derived pressure in station height [hPa]
    I_infty = 1367;                    % solar constant  
    l       = 'infinite';              % Length of street fixed length [m];
    %                                    currently not not fully implemented
    h_man   = 1.1;                     % height of the center point of a person
    %
    % set fixed width in case of open area for view factor calculation
    %if h == 0
    %    w = 100;
    %end
    %
    %
    %% DIRECT RADIATION + STREET DESIGN PARAMETERS
    % astronomic parameters related to sw radiation
    [alpha_sun, psi_sun] = se_calc_astronomic(jd,hh,phi,basis_declin_calc);
    
    % sw radiation reaching ground after absorption in atm 
    [R_sw,I_0] = se_calc_sw_ground(I_infty, alpha_sun,jd, pb, p0, TLam2,basis_dir_rad);
    
    % calculation of fraction of lit and shaded part of street canyon
    [frac_wall, frac_floor, is_pos_lit, cos_eta,ill_wall] ...
        = se_calc_illuminated(h, w, ori, pos_rel, psi_sun, alpha_sun);
    
    % Estimate surface-to-surface view factors
    [vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe] ...
        = se_calc_vf_a2a(h,w,frac_floor,frac_wall);
    
    % person to surface view factors
    [vfp2] = se_calc_vf_p2a(h,h_man,w,l,pos_rel,frac_wall,frac_floor,ill_wall,basis_pvf);
    
    % direct solar radiation reaching the human body
    [I_t, f_p] = se_calc_dir_man(R_sw,alpha_sun, is_pos_lit);
    %
    %% DIFFUSE RADIATION
    
    % diffuse solar radiation from sky
    D_s = se_calc_diff_sky(R_sw,alpha_sun,pb,p0,TLam2,I_0,basis_diff_rad);
    
    % total diffuse solar radiation reaching the human body:
    % 1) sky
    % 2) reflected (by walls and ground based on part that is lit)
    [D_t] =...
        se_calc_diff_man(D_s,R_sw,alpha_w,alpha_g,vfp2,...
        vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe,cos_eta);

    %% LONGWAVE RADIATION
    
    % longwave radiation from sky
    E_s = se_calc_lon_sky(e_2mPa,T_a2m,sigma, basis_lw_sky);
    
    % longwave radiation from ground and walls
    [E_g, E_w,T_g,T_w]=se_calc_lon_morph(basis_ts,T_g,T_w,...
        T_a2m,epsilon_g,epsilon_w,sigma,frac_wall,frac_floor,E_s,...
        D_s,alpha_g,R_sw,cos_eta,ill_wall,...
        alpha_w,v_10,bo, h,...
        vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe);
    
    % total longwave radiation reaching the human body:
    % 1) sky
    % 2) ground
    % 3) walls
    % 4) (reflected at walls + ground) depends on used version
    E_t = se_calc_lon_man(E_s,E_g,E_w,epsilon_w,epsilon_g,vfp2,...
                          vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe);
    
    %    
    %% T_MRT [K]
    tmrt = (1/sigma*(E_t + alpha_k/epsilon_p*(D_t + I_t))).^0.25;
    %% IO
    varargout{1}=is_pos_lit;
    if l_dispValues
        lw  = E_t;
        dfs = alpha_k/epsilon_p*(D_t);
        dis = alpha_k/epsilon_p*(I_t);
        ges = E_t+alpha_k/epsilon_p*(D_t+I_t);
        varargout{2}=lw;
        varargout{3}=dfs;
        varargout{4}=dis;
        varargout{5}=ges;
        varargout{6}=T_g;
        varargout{7}=T_w;
        %glob
        varargout{8}=is_pos_lit*R_sw*cos_eta(2)+D_s;
        % diffuse radiation from sky
        varargout{9}=D_s;
        % longwave downward radiation
        varargout{10}=E_s;
        % direct radiation
        varargout{11}=R_sw;
        % SW from ground
        varargout{12}= varargout{8}*alpha_g;
        % LW from ground
        varargout{13}=E_g;
        varargout{14}=ill_wall;
        varargout{15}=f_p;
        varargout{16}=cos_eta;
        varargout{17}=alpha_sun;
        varargout{18}=vfp2;
        varargout{19}=vf_shg;
        varargout{20}=vf_ilg;
        varargout{21}=vf_ilw;
        varargout{22}=vf_shw;
        varargout{23}=vf_shwe;
    end
    %
end
