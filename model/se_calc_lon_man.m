function[E_t]=se_calc_lon_man(E_s,E_g,E_w,epsilon_w,epsilon_g,vfp2,...
        vf_shg, vf_ilg, vf_ilw, vf_shw, vf_shwe)

    % SE_CALC_LON_MAN       Calculates longwave radiation reaching the 
    %                           human body
    %
    %  INPUT:
    %
    %    E_s             -   Longwave radiation from sky [Wm^-2]
    %    E_g             -   Longwave radiation from ground [Wm^-2]
    %    E_w             -   Longwave radiation from walls [Wm^-2]
    %    epsilon_w       -   Emissivity of the wall
    %    epsilon_g       -   Emissivity of the ground
    %    vfp2            -   View Factor person to wall, ground, sky
    %    vf_shg          -   View Factors for shaded ground to other
    %                        surfaces
    %    vf_ilg          -   View Factors for lit ground to other surfaces
    %    vf_ilw          -   View Factors for lit wall to other surfaces
    %    vf_shw          -   View Factors for shaded wall to other surfaces
    %    vf_shwe         -   View Factors for entirley shaded wall to other
    %                        surfaces
    %
    %  OUTPUT:
    %
    %    E_t    -   total longwave radiation reaching human body [Wm^-2]
    %
    %%
    %
    E_t = vfp2.w.lit  * E_w (1) + vfp2.w.sha * E_w(2) ...
        + vfp2.w.shae * E_w (3) ...
        + vfp2.g.lit  * E_g(1)  + vfp2.g.sha * E_g(2) ...
        + vfp2.s      * E_s ...
        + vfp2.g.lit  * vf_ilg.s * (1-epsilon_g) * E_s ...
        + vfp2.g.sha  * vf_shg.s * (1-epsilon_g) * E_s ...
        + vfp2.w.lit  * vf_ilw.s * (1-epsilon_w) * E_s ...
        + vfp2.w.sha  * vf_shw.s * (1-epsilon_w) * E_s...
        + vfp2.w.shae * vf_shwe.s* (1-epsilon_w) * E_s;
end
