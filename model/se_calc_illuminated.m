function [frac_wall, frac_floor, is_pos_lit, cos_eta, ill_wall] = ...
    se_calc_illuminated(h, w, ori, pos_rel, psi_sun, alpha_sun)

    % SE_CALC_ILLUMINATED    Calculates what is illuminated by the sun
    %
    %  INPUT:
    %
    %    h          -   Building height [m]
    %    w          -   Building width [m]
    %    ori        -   Orientation of the street with respect to north [°]
    %    pos_rel    -   Position within street canyon [0 < pos_rel < 1]
    %    psi_sun    -   Azimuth angle of the sun [rad]
    %    alpha_sun  -   Elevation angle of the sun [rad]
    %
    %
    %  OUTPUT:
    %
    %    frac_wall  -   fraction of wall that is illuminated
    %    frac_floor -   fraction of floor that is illuminated
    %    is_pos_lit -   Is position of person currently lit (boolean)
    %    cos_eta    -   cos(eta) angle for inclined surfaces
    %    ill_wall   -   Illuminated wall ('r', 'l', 'n')
    %
    %  ASSUMPTIONS:
    %
    %    1. street canyon is symmetric
    %    2. Nothern hemisphere (eg. in a N-S street right side of street
    %                           receives radiation earlier)
    %    3. Infinite street length
    %
    %% DEFINITION OF D2R
    %  because: matlab intrinsic function deg2rad is slow.
    d2r = (pi()/180);
    r2d = (180/pi());
    %
    %% Set defaults in case of night
    frac_floor   = 0;
    frac_wall    = 0;
    is_pos_lit   = false;
    ill_wall     = 'n';
    cos_eta(1:2) = 9999;
    %
    %% onle relevant during the day
    if alpha_sun > 0
        %
        % FRACTION OF FLOOR AND WALL LIT
        phi = r2d*psi_sun - ori + 90;
        x   = w / abs(cos(d2r*phi)); % cos und 90 sind hier stark gekoppelt, möglich wären auch sin und 0.
        tan_alpha_s = tan(alpha_sun); % ratio h / x
        if (mod(r2d*psi_sun-ori,180)==0)
            frac_wall  = 0;
            frac_floor = 1;
        else
            if (tan_alpha_s < (h) / x)
                % case: floor is dark
                frac_floor = 0;
                frac_wall = tan_alpha_s * x / (h);
            else
                % case: wall is lit
                frac_wall = 1;
                %
                if x ~= inf
                    frac_floor = 1 - h / (tan_alpha_s * x);
                else
                    % sun shines directly into the canyon
                    frac_floor = 1;
                    frac_wall  = 0;
                end
            end
        end
        %
        tol = 1e-10;
        is_pos_lit = (abs(frac_floor-(0.5 + (0.5 - pos_rel) * sign(cos(d2r*phi))))<tol) ... % testing equality
        || (frac_floor >= double((0.5 + (0.5 - pos_rel) * sign(cos(d2r*phi)))));  % testing greater
        %
        %% AZIMUTH AND ELEVATION ANGLE OF WALL
        slope_angle(1) = 90; % wall
        slope_angle(2) = 0;  % ground
        %
        % azimuth of slope
        rel_angl = ori - r2d*psi_sun;
        if rel_angl > 360
            rel_angl = wrapTo360(rel_angl);
        end
        % 1: wall
        %    ill_wall with respect to a N-S oriented canyon
        if rel_angl < 0
            psi_wal(1) = ori+90;
            ill_wall   = 'r';
        elseif rel_angl > 0 && rel_angl < 180
            psi_wal(1) = (mod(ori-90 +180,360)-180);
            ill_wall   = 'l';
        elseif rel_angl > 180 && rel_angl < 360
            psi_wal(1) = mod(ori+90+180,360)-180;
            ill_wall   = 'r';
        else
            % if the sun shines directly into the canyon
            psi_wal(1) = 0;
            ill_wall   = 'n';
        end
        % 2: ground
        psi_wal(2) = r2d*psi_sun;
        %
        % cos eta
        cos_eta = [0,0];
        if ill_wall ~= 'n'
            cos_eta(1) = cos(d2r*slope_angle(1))*sin(alpha_sun) + ...
                         sin(d2r*slope_angle(1))*cos(alpha_sun)*...
                         cos(d2r*psi_wal(1)-psi_sun);        
        end
        cos_eta(2) = cos(d2r*slope_angle(2))*sin(alpha_sun) + ...
                     sin(d2r*slope_angle(2))*cos(alpha_sun)*...
                     cos(d2r*psi_wal(2)-psi_sun);
    end
    %
end
