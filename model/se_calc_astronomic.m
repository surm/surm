function [alpha_sun, psi_sun, varargout] = se_calc_astronomic(...
    jd,hh,phi,basis_declin_calc)

    % SE_CALC_ASTRONOMIC    Calculates astronomic parameters of direct
    %                       radiation
    %
    %  INPUT:
    %
    %    jd                -   Julian day number
    %    hh                -   hour of interest
    %    phi               -   Geographical latitude [°]
    %    basis_declin_calc -   Calculation Method for declination, valid are
    %                          {MITRAS,vdi3789}
    %   
    %
    %  OUTPUT:
    %   
    %    alpha_sun        -   Elevation angle of the sun [rad]
    %    psi_sun          -   Azimuth angle of the sun [rad]
    %    cos_zenit_sun    -   Cosine of zenith angle of the sun []
    %
    %  REFERENCES
    %
    %    Schluenzen, K. H., D.D. Flagg, B.H. Fock, A. Gierisch, C. Lüpkes, 
    %      V. Reinhardt, C. Spensberger, 2012: Scientific Documentation of 
    %      the Multiscale Model System M-SYS, Meteorological Insitute, 
    %      University of Hamburg, MEMI Technical Report 4, P. 94 
    %
    %    VDI 3789, Part 3 (2001): Environmental meteorology - Interactions
    %      between atmosphere and surfaces: Calculation of spectral
    %      irradiances in the solar wavelength range
    %
    %
    %% DEFINITION OF D2R
    %  because: matlab intrinsic function deg2rad is slow. 
    d2r = (pi()/180);
    r2d = (180/pi());

    %% CALCULATION OF HOUR ANGLE FOR specific hour
    omega = ((hh - 12) * 15); % from VDI3789, Part 3 p 54
    
    %% CALCULATION OF EARTH'S DECLINATION
    
    switch basis_declin_calc
        case lower('MITRAS')
            jd_rad = 2*pi*(jd-1)/365; 
            delta = 0.006918 ...
                  - 0.399912 * cos(jd_rad)   + 0.070257 * sin(jd_rad)...
                  - 0.006758 * cos(2*jd_rad) + 0.000907 * sin(2*jd_rad) ...
                  - 0.002697 * cos(3*jd_rad) + 0.001480 * sin(3*jd_rad);
        case lower('vdi3789')
            delta = (asin(0.3978 * sin(d2r*((0.9856 * jd - 2.72) - 77.51...
                + 1.92 * sin(d2r*(0.9856 * jd - 2.72))))));
        otherwise 
            error('You used an unknown value for declin_calc')
    end
    
    %% CALCULATION OF ELEVATION ANGLE
    sinalpha = sin(delta).*sin(d2r*phi) + cos(delta).*cos(d2r*phi).*cos(d2r*omega);   
    
    %% CALCULATION OF AZIMUTH ANGLE
    %
    % here pi/2-zenit is used for alpha_sun, because alpha_sun is set to 0
    % during the night already
    cos_psi = (sinalpha.*sin(d2r*phi)-sin(delta)) ...
             ./ (cos(asin(sinalpha)).*cos(d2r*phi));
    
    % for the correct sign of psi
    psi_sun = acos(cos_psi).*sign(omega);
    
    %% ADJUSTMENT OF ELEVATION ANGLE DURING THE NIGHT
    alpha_sun = asin(sinalpha); 
    
    %% OPTIONAL OUTPUT: TIME OF SUNRISE
    hh_sr = (((acos(sin(1*pi)-sin(delta).*sin(d2r*phi)/(cos(delta).*cos(d2r*phi))))*r2d)/15+12-24)*-1;
    hh_ss = ((acos(-sin(delta).*sin(d2r*phi)/(cos(delta).*cos(d2r*phi))))*r2d)/15+12;
    varargout{1} = hh_sr;
    varargout{2} = hh_ss;
end
