function[f_p]=se_calc_fp(alpha_sun)

    % SE_CALC_FP       Calculates the projection factor for a rotational
    %                      symmetric person
    %
    %  INPUT:
    %
    %    alpha_sun  -   Elevation angle of the sun [rad]
    %   
    %
    %  OUTPUT:
    %
    %    f_p        -   Projection factor for specific elevation angle
    %
    %  REFERENCES: 
    %
    %    Matzarakis, A., F. Rutz, H. Mayer, 2010: Modelling radiation 
    %       fluxes in simple and complex environments: basics of the rayman 
    %       model. – International Journal of Biometeorology 54(2), 131–139.
    %
    %    VDI, 2008: Methods for the human biometeorological evaluation of 
    %      climate and air quality for urban and regional planning at 
    %      regional level, Part I: Climate VDI 3787 Part 2, Beuth, Berlin.
    %
    %
    %% DEFINITION OF D2R
    %  because: matlab intrinsic function deg2rad is slow. 
    d2r = (pi()/180);
    r2d = (180/pi());
    %%
    f_p = 0.308 .* cos( d2r*(...
                r2d*alpha_sun .*(0.998-(r2d*alpha_sun).^2/50000)));
            
end
