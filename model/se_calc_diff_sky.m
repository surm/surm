function[D_s]=se_calc_diff_sky(R_sw,alpha_sun,p,p0,TLam2,I_0,basis_diff_rad)

    % SE_CALC_DIFF_SKY      Calculates diffuse radiaton from sky
    %
    %  INPUT:
    %
    %    R_sw                -   SW from sky reaching ground without 
    %                            consideration of city's morphology for 
    %                            different times
    %    alpha_sun           -   Elevation angle of the sun [rad]
    %    p                   -   Pressure
    %    p0                  -   Mean sea level pressure
    %    TLam2               -   Linke turbidity factor of relative optical
    %                            air mass 2
    %    I_0                 -   solar constant with accounted annual cycle
    %    basis_diff_rad      -   Define basis of diffuse SW parameterization
    %                            applied here (valid are: 'MITRAS',
    %                            'Bruse1999', 'vdi1994', 'esra')
    %   
    %
    %  OUTPUT:
    %   
    %    D_s        -   Diffuse radiation from sky
    %
    %  REFERENCES
    %
    %    Bruse, M., 1999: Die Auswirkungen kleinskaliger Umweltgestaltung 
    %       auf das Mikroklima – Entwicklung des prognostischen numerischen 
    %       Modells ENVI-met zur Simulation der Wind-, Temperatur- und 
    %       Feuchteverteilung in städtischen Strukturen Ph.D. thesis, 
    %       Geowissenschaften, Ruhr-Universität Bochum, Bochum, Germany. 
    %       p. 53
    %
    %    Gierisch, A., 2011: Mikroskalige Modellierung meteorologischer und
    %       anthropogener Einflüsse auf die Wä̈rmeabgabe eines Gebäudes 
    %       Master’s thesis, Geowissenschaften, University of Hamburg, 
    %       Hamburg, Germany, p. 12
    %
    %    Rigollier C, Bauer O, Wald L (2000): On the clear sky model of the
    %       ESRA - European Solar Radiation Atlas - with respect to the
    %       HELIOSAT method. Solar Energy, 68 (1): 33 - 48
    %
    %    VDI 3789, 2 (1994): Interactions between Atmosphere and Surfaces
    %      Calculation of Short-wave and Long-wave Radiation
    %
    %%
    switch basis_diff_rad
        case lower('MITRAS')
        % Derive diffuse part from direct shortwave radiation radiation
            gamma = 1./(1+8*(sin(alpha_sun)).^0.7);
            %
            % warning is issued for high latitudes, when division by 0
            % Comment: sin(alpha_sun) is here part of the parameterization to derive
            %    the amount of diffuse radiation from direct radiation and does not
            %    depend on the elevation angle of the surface
            D_s   = R_sw.*sin(alpha_sun).*(gamma/(1-gamma));
        case lower('vdi1994')
        % Derive diffuse part as difference from global radiation
            TL  = TLam2*p/p0;
            G   = 0.84 * I_0 * sin(alpha_sun) * exp(-0.027*TL/sin(alpha_sun));
            D_s = G - R_sw * sin(alpha_sun);
        case lower('esra')
        % Estimate clear sky independent of direct radiation
            TL  = TLam2*p/p0;
            %
            Trd = -0.015843 + 0.030543 * TL + 0.0003797 * TL^2;
            %
            a0  = 0.26463-0.061581*TL+0.0031408*TL^2;
            if a0*Trd < 0.002
               a0 = 0.002/Trd; 
            end
            a1  = 2.04020  + 0.018945  * TL - 0.0111610 * TL^2;
            a2  = -1.33025 + 0.0392311 * TL + 0.0085079 * TL^2;
            F_d = a0 + a1*sin(alpha_sun)+a2*(sin(alpha_sun))^2;
            %        
            D_s = I_0*Trd*F_d;
    end
    if D_s < 0 || isnan(D_s)
       D_s = 0;
    end
end
