function [E_s] = se_calc_lon_sky(e_2mPa,T_a2m,sigma,basis_lw_sky)

    % SE_CALC_LON_SKY       Calculates longwave radiation from sky
    %
    %  INPUT:
    % 
    %    e_2mPa          -   Water vapour pressure in 2m in [Pa]
    %    T_a2m           -   Temperature in 2m [K] 
    %    sigma           -   Stefan-Boltzman-Konstant [Wm^-2K^-4]
    %    basis_lw_sky    -   Calculation method of LW from sky
    %   
    %
    %  OUTPUT:
    %
    %    E_s    -   Longwave radiation from sky [Wm^-2]
    %
    %  REFERENCES: 
    %
    %    Bolz HM, Falckenberg G (1949): Neubestimmung der Konstanten der 
    %       Angstroemschen Strahlungsformel. Z. für Meteorologie, 3 (4): 
    %       97-100.
    %
    %    Gierisch, A., 2011: Mikroskalige Modellierung meteorologischer und
    %       anthropogener Einflüsse auf die Wä̈rmeabgabe eines Gebäudes 
    %       Master’s thesis, Geowissenschaften, University of Hamburg, 
    %       Hamburg, Germany
    %
    %    Idso SB (1981): A set of equations for full spectrum and 8- to 14-µm 
    %       and 10.5- to 12.5-µm thermal radiation from cloudless skies. 
    %       Water Resources Research, 17 (2): 295-304.
    %
    %    VDI 3789, 2 (1994): Interactions between Atmosphere and Surfaces
    %      Calculation of Short-wave and Long-wave Radiation
    %
    %%
    switch basis_lw_sky
        case lower('MITRAS')
            % also used by RayMan (Matzarakis 2010)
            % CONSTANTS from Bolz und Falckenberg (1949), primary valid for
            % German Coast
            a      = 0.820;
            b      = 0.250;
            c      = 0.126;
            afphag = 1/100*3/4; %Conversion constant Pa -> mmHg; 760/101325

            % CALCULATION:
            e_2mHg = e_2mPa*afphag;
            E_s    = sigma*T_a2m^4*(a-b*10^(-c*e_2mHg)); 
        case lower('Idso1981')
            %from Oke 1987, p 373
            E_s    = sigma*T_a2m^4*(0.70+5.95*10^(-5)*(e_2mPa/100)*exp(1500/T_a2m));
        case lower('vdi3789')
            % p. 26
            epsilon_eff = 9.9*10^(-6)*T_a2m^2;
            E_s         = sigma*T_a2m^4*epsilon_eff;
        otherwise
            disp('Wrong input for basis_lw_sky')
    end
end
