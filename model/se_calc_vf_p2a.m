function[vfp2]=se_calc_vf_p2a(h,h_man,w,l,pos_rel,frac_wall,frac_floor,ill_wall,BASIS_pvf)

    % SE_CALC_VF_P2A    Calculates view factors of a person to different 
    %                           surfaces
    %
    %  INPUT:
    %
    %    h          -   Building height
    %    h_man      -   Person height
    %    w          -   Street width
    %    l          -   Street length
    %    pos_rel    -   Position within street canyon
    %    frac_wall  -   Lit fraction of wall
    %    frac_floor -   Lit fraction of ground
    %    ill_wall   -   Lit wall (left, right, no)
    %    BASIS_pvf  -   Type of view factor calcualtion for a person
    %   
    %
    %  OUTPUT:
    %
    %    vfp2.w       -   person to wall view factors (lit, sha, shae)
    %    vfp2.g       -   perston to ground view factor (lit, sha) 
    %    vfp2.s       -   person to sky view factor
    %
    %  ASSUMPTIONS:
    %
    %    1. street canyon is symmetric
    %    2. Nothern hemisphere (eg. in a N-S street right side of street
    %                           receives radiation earlier)
    %    
    %% CALCULATION OF STREET DESIGN PARAMETERS    
    % effective building height: Actual building height smaller than h, 
    % because parting plane at h_man
    if frac_wall == 0 && frac_floor == 0
        vfp2.g.sha  = 0.5;
        vfp2.g.lit  = 0;
    else
        vfp2.g.sha  = 0;
        vfp2.g.lit  = 0.5;
    end
    vfp2.s      = 0.5;
    vfp2.w.shae = 0;
    vfp2.w.sha  = 0;
    vfp2.w.lit  = 0;
    %
    if h == 0
       return
    else
        switch ill_wall
            case 'r'
                dis_to_w_lit = pos_rel*w;
                dis_to_w_sha = (1-pos_rel)*w;
            case {'l','n'} % for n distances not relevant as both walls shaded, see se_calc_lon_morph, se_calc_vf_a2a
                dis_to_w_lit = (1-pos_rel)*w;
                dis_to_w_sha = pos_rel*w;
        end
        %
        % Effective street length (sufficient for vf calculation)
        switch l
            case 'infinite'
                loi=min(dis_to_w_lit,dis_to_w_sha)*70;
            otherwise
                loi = l;
        end
    end
    %

    %% CALCLATION OF VIEW FACTORS FOR LIT OR PARTLY LIT WALL
    gridspacing          = 0.5;
    vfp2.w.shae          = 0;
    vfp2.w.sha           = 0;
    vfp2.w.lit           = 0;
    vf_wall_above_center = 0;
    %
    h_eff = h-h_man;    
    %
    if h_eff > 0 % wall above barycenter of human
        vf_wall_above_center = se_calc_vfperson(loi,h_eff,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
    end
    vf_wall_below_centre = se_calc_vfperson(loi,h_man,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
    %
    if frac_floor == 1 % sun parallel to canyon
        vfp2.w.lit = 0;
        vfp2.w.sha = vf_wall_above_center + vf_wall_below_centre;
    else
        if (h_eff > 0) % wall higher than barycenter of human
            if (h*frac_wall <= h_eff) % only upper part of wall lit
                % upper part of wall (above barycenter)
                vf_wall_above_centre2_lit = se_calc_vfperson(loi,h*(1-frac_wall)-h_man,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
                if isnan(vf_wall_above_centre2_lit) || isinf(vf_wall_above_centre2_lit)
                    vf_wall_above_centre2_lit=0;
                end
                vfp2.w.lit = vf_wall_above_center - vf_wall_above_centre2_lit;
                vfp2.w.sha = vf_wall_above_centre2_lit + vf_wall_below_centre;
            else
                % lower part of wall (below barycenter)
                vf_wall_below_centre2_lit = se_calc_vfperson(loi,h*frac_wall-h_eff,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
                vfp2.w.lit = vf_wall_above_center + vf_wall_below_centre2_lit;
                vfp2.w.sha = vf_wall_below_centre - vf_wall_below_centre2_lit;  
                if (vfp2.w.sha < 10-8)
                    vfp2.w.sha = 0;
                end
            end
        else  % wall smaller than baycenter of human
            vf_wall_below_centre2_lit     = se_calc_vfperson(loi,h*frac_wall+h_man-h,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
            vf_wall_below_center_uptowall = se_calc_vfperson(loi,h_man-h,dis_to_w_lit,'ver',BASIS_pvf,gridspacing,gridspacing);
            vfp2.w.sha = vf_wall_below_centre      - vf_wall_below_centre2_lit;
            vfp2.w.lit = vf_wall_below_centre2_lit - vf_wall_below_center_uptowall;
        end
        %
        %% CALCULATION OF VIEW FACTORS FOR ENTIRELY SHADED WALL
        if h_eff > 0 % wall above barycenter of human
            vf_w_top_shae = se_calc_vfperson(loi,h_eff,dis_to_w_sha,'ver',BASIS_pvf,gridspacing,gridspacing);
            vf_w_bot_shae = se_calc_vfperson(loi,h_man,dis_to_w_sha,'ver',BASIS_pvf,gridspacing,gridspacing);
        else
            vf_w_top_shae = 0;
            vf_w_bot_shae = vf_wall_below_centre- se_calc_vfperson(loi,h,dis_to_w_sha,'ver',BASIS_pvf,gridspacing,gridspacing);
        end
        vfp2.w.shae = vf_w_top_shae + vf_w_bot_shae;
        %
    end
    %% CALCULATION OF GROUND VIEW FACTORS
    %
    % vf in direction of lit or shaded wall
    vf_g_lit = se_calc_vfperson(dis_to_w_lit,loi,h_man,'hor',BASIS_pvf,gridspacing,gridspacing);
    %
    vf_g_sha = se_calc_vfperson(dis_to_w_sha,loi,h_man,'hor',BASIS_pvf,gridspacing,gridspacing);
    %
    if (frac_floor*w < dis_to_w_lit)
        vf_g_uptolit = se_calc_vfperson(dis_to_w_lit-frac_floor*w,loi,h_man,'hor',BASIS_pvf,gridspacing,gridspacing);
        vfp2.g.lit = vf_g_lit - vf_g_uptolit;
        vfp2.g.sha = vf_g_uptolit + vf_g_sha;
    else
        if frac_floor == 1 % sun parallel to canyon
            vfp2.g.lit = vf_g_lit + vf_g_sha;
            vfp2.g.sha = 0;
        else
            vf_g_uptosha = se_calc_vfperson(frac_floor*w-dis_to_w_lit,loi,h_man,'hor',BASIS_pvf,gridspacing,gridspacing);
            vfp2.g.lit = vf_g_lit + vf_g_uptosha;
            vfp2.g.sha = vf_g_sha - vf_g_uptosha;
        end
    end
    %
    %% CALCULATION OF SKY VIEW FACTOR
    vfp2.s = 1 - vfp2.g.lit - vfp2.g.sha - vfp2.w.lit - vfp2.w.sha - vfp2.w.shae;
    if vfp2.s < 0
       error('Something is wrong with vfp2s. Cannot be < 0.')
    end
end
