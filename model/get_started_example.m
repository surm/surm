%% Example program to set-up a Tmrt calculation with SURM
%
% This program shows how to set different input variables for SURM
% and how to call the main SURM calculation
%
% For details on the calculation of Tmrt and the input parameter see 
% the main of SURM: calcTmrt
%
%% USER INPUT SECTION
clear all
%
%------- street design ------
%
h         =  10;    % building height [m]
w         =  10;    % street width    [m]
ori       =  57;    % steet angle towards north [°]
%
%------- Built-up environment properties ------
%
T_w_init  = 293.15; % wall temperature [K]
T_g_init  = 293.15; % ground temperature [K]
epsilon_w = 0.9;    % emissivity of wall
epsilon_g = 0.95;   % emissivity of ground
alpha_w   = 0.15;   % mean wall albedo
alpha_g   = 0.15;   % mean ground albedo
%
%------- Metorological input --------
%
T_a2m     = 20 + 273.15; % air temperature in 2m [K]
RH_a2m    = 50;          % Relative humidity in 2m [%]
z_nn      = 11;          % Station height [m]; e.g. Hamburg, Fuhlsbuettel = 11 m
%
%------- Radiation input --------
%
phi       = 53;          % latitude [°]
year      = 2015;        % ATTENTION: No leap years!
month     = 6;           % Month to simulate
day       = 21;          % Day to simulate
hour      = 12;          % Hour to simulate
fileID    = fopen('tlam2.txt'); % Linke turbidity factor taken from http://www.soda-pro.com/web-services/atmosphere/linke-turbidity-factor-ozone-water-vapor-and-angstroembeta
tlam2_cell= textscan(fileID,'%4s %f','HeaderLines',7,'Delimiter',' '); 
%
%------ human characteristics input ------
%
alpha_k   = 0.7;  % absorption coefficient of the irradiated body surface 
                  % for short-wave radiation (default: 0.7)
epsilon_p = 0.97; % absorption coefficient for long-wave radiation (= emissivity) of the human body (default: 0.97)
pos_rel   = 0.5;  % position within the street canyon as seen from right to
                  % left in a N-S street canyon, valid are values 
                  % between 0 and 1
%
%------ Optional inputs ------
%
% For a description of optional inputs see calcTmrt
%
%% ############# END OF USER INPUT SECTION ##############################
%
% conversions
% 1) julian day in radian
jd     = datenum(year,month,day) - datenum(year, 1, 1) + 1; 
%
% 2) RH in e using magnus equation
e_2mPa = ...
         (RH_a2m*6.112*exp(17.62*(T_a2m-273.15)/(243.12+(T_a2m-273.15))))...
         /100*100;
%
% 
% calculation of tmrt
tic % to measure calculation time
tmrt = calcTmrt(h,w,ori,...
            epsilon_w,epsilon_g,alpha_w,alpha_g,...
            T_a2m,e_2mPa,...
            phi,jd,hour,...
            alpha_k,epsilon_p,pos_rel,...
            'T_w',T_w_init,'T_g',T_g_init,...
            'TLam2',tlam2_cell{1,2}(month),'z_nn',z_nn)
toc
