function [vf] = se_vfp2pp(w1,l1,l2,l3,l4)
    %
    % SE_VFP2PP    Calculates View Factor F1-2 of two infinite long parallel 
    %                  plates with different widths
    %
    %  INPUT:
    %
    %    w1                -   width of plate 1
    %    l1                -   cross sting length 1
    %    l2                -   cross sting length 2 (not shown below)
    %    l3                -   edge length 1
    %    l4                -   edge length 2
    %   
    %
    %               w2
    %            --------
    %           /\     / l4
    %          /  \l1 / 
    %      l3 /    \ /
    %         -------
    %           w1
    
    %  OUTPUT:
    %   
    %    vf               -   view factor from plate 1 to plate 2
    %
    %  REFERENCES
    %
    %    Howell, J. R. 2010: A Catalog of Radiation Heat Transfer 
    %      Configuration Factors, 3rd Edition, 
    %      http://www.thermalradiation.net/sectionc/C-2a.htm, 
    %      accessed 18.10.2017
    %
    %% 
    vf = (l1 + l2 - l3 - l4) / (2 * w1);
end