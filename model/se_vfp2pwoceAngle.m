function [vf] = se_vfp2pwoceAngle(x1,x2,y1,y2,alpha)
    %
    % SE_VFP2PWOCEANGLE    Calculates View Factor of two infinite long 
    %                          plates of unequal width including angle 
    %                          alpha without common edge
    %
    %  INPUT:
    %
    %    x1                -   distance from origin to plate 1
    %    x2                -   distance from origin to end of plate 1
    %    y1                -   distance from origin to plate 2
    %    y2                -   distance from origin to end of plate 2
    %    alpha             -   angle between thte two plates [rad]
    %
    %
    %            /
    %     y2-y1 /
    %          /
    %         . `  alpha
    %      y1.   `
    %       ......`...------
    %           x1     x2-x1
    %       |       x2     |
    %
    %
    %  OUTPUT:
    %   
    %    vf               -   view factor F1-2
    %
    %  REFERENCES
    %
    %    Howell, J. R. 2010: A Catalog of Radiation Heat Transfer 
    %      Configuration Factors, 3rd Edition, 
    %      http://www.thermalradiation.net/sectionc/C-5a.html, 
    %      accessed 18.10.2017
    %
    %% 
    vf = (sqrt(x1^2 - 2*x1*y2*cos(alpha) + y2^2) ...
         +sqrt(x2^2 - 2*x2*y1*cos(alpha) + y1^2)...
         -sqrt(x2^2 - 2*x2*y2*cos(alpha) + y2^2)...
         -sqrt(x1^2 - 2*x1*y1*cos(alpha) + y1^2)) / (2*(x2 - x1)) ;
end