function[I_t,f_p]=se_calc_dir_man(R_sw,alpha_sun,is_pos_lit)

    % SE_CALC_DIR_MAN       Calculates SW-radiation, which reaches
    %                           the human body using a projection factor
    %                           (= weighting function)
    %
    %  INPUT:
    % 
    %    R_sw            -   SW direct radiation reaching ground after
    %                        absorption during different times of the day
    %    alpha_sun       -   Elevation angle of the sun [rad]
    %    is_pos_lit      -   Is position of person currently lit (boolean)
    %   
    %
    %  OUTPUT:
    %
    %    I_t    -   Direct Radiation reaching the human body
    %    f_p    -   Projection factor
    %
    %  REFERENCES: 
    %
    %    VDI, 2008: Methods for the human biometeorological evaluation of 
    %      climate and air quality for urban and regional planning at 
    %      regional level, Part I: Climate VDI 3787 Part 2, Beuth, Berlin.
    %
    %
    %% 
    f_p = se_calc_fp(alpha_sun);
    %
    I_t=f_p.*is_pos_lit.*R_sw;
end
