function[T_w, T_g, v_10, TLam2, bo, z_nn, ...
    basis_declin_calc, basis_dir_rad, basis_diff_rad, ...
    basis_lw_sky, basis_ts, basis_pvf, l_dispValues] = se_parse_Input2(varargin)
    %
    % SE_PARSE_INPUT       Parses input of tmrt calculation
    %
    %  INPUT:
    % 
    %    varargin        -  optional arguments for calcTmrt
    %
    %  OUTPUT:
    % 
    %    T_w             -   Temperature of sunny wall (mean) [K]
    %    T_g             -   Temperature of sunny ground (mean) [K]
    %    v_10            -   wind speed in 10 m, only used if
    %                        basis_ts, to estimate wall and ground
    %                        temperature [m/s]
    %    TLam2           -   Linke turbidity factor of relative optical
    %                        air mass 2 can be taken from 
    %                        http://www.soda-pro.com/web-services/atmosphere/linke-turbidity-factor-ozone-water-vapor-and-angstroembeta
    %    bo              -   Bowen ratio: 0.5 grass, 1.3 urban
    %    z_nn            -   Station height above sea level [m]
    %
    %    basis_declin_calc-  Type of declination calculation (valid are: 
    %                        'MITRAS','vdi3789')
    %    basis_dir_rad   -   Define basis of short wave absorption
    %                        applied here (valid are: 'MITRAS',
    %                        'vdi1994','esra')
    %    basis_diff_rad  -   Define basis of diffuse SW parameterization
    %                        applied here (valid are: 'MITRAS',
    %                        'vdi1994','esra')
    %    basis_lw_sky    -   Define basis of long wave from sky
    %                        (valid are: 'MITRAS', 'Idso1981', 'vdi3789')
    %    basis_ts        -   Handling of surface temperatures of ground and
    %                        walls ('prescribed' (prescribed values for
    %                        sun-lit wall and ground during the day and
    %                        entire surfaces during the night) and 
    %                        'diagnostic' (diagnostic calculation of ts)).
    %    basis_pvf       -   Define basis of view factor calculation for person
    %                        (valid are: 'Fanger',
    %                        'Fangerfast','Cannistraro')
    %    l_dispValues    -   Logical: Show calculated values of different
    %                        terms
    %
    %% Definitions
    %
    expected_declin_calc= {lower('MITRAS'),lower('vdi3789')};
    expected_diff_rad   = {lower('MITRAS'),lower('vdi1994'),lower('esra')};
    expected_dir_rad    = {lower('MITRAS'),lower('vdi1994'),lower('esra')};
    expected_lw_sky     = {lower('MITRAS'), lower('Idso1981'), lower('vdi3789')};
    expected_ts         = {lower('prescribed'), lower('diagnostic')};
    expected_pvf        = {lower('Fanger'), lower('Fangerfast'),lower('Cannistraro')};
    % Defaults
    %
    options = struct(lower('BASIS_DECLIN_CALC'),lower('vdi3789'),...
                     lower('BASIS_DIFF_RAD'),lower('esra'),...
                     lower('BASIS_DIR_RAD'),lower('esra'),...
                     lower('BASIS_LW_SKY'),lower('MITRAS'),...
                     lower('BASIS_TS'),lower('prescribed'),...
                     lower('BASIS_PVF'),lower('Fangerfast'),...
                     lower('l_dispValues'),false,...
                     lower('T_w' ),nan,lower('T_g'  ),nan,...
                     lower('v_10'),nan,lower('TLam2'),nan,...
                     lower('bo'  ),nan,lower('z_nn' ),nan);
    % read the acceptable names
    optionNames = fieldnames(options);

    % count arguments
    nArgs = length(varargin{:});
    if round(nArgs/2)~=nArgs/2
       error('CALCTMRT needs propertyName/propertyValue pairs as inputs.')
    end
    %
    % read variable-value pairs
    for pair = reshape(varargin{:},2,[])
       inpName  = lower(pair{1});
       inpValue = lower(pair{2});
       if any(strcmp(lower(inpName),lower(optionNames)))
          % check input is valid
          switch inpName
              case lower('BASIS_DECLIN_CALC')
                  expected_values = expected_declin_calc;
              case lower('BASIS_DIFF_RAD')
                  expected_values = expected_diff_rad;
              case lower('BASIS_DIR_RAD')
                  expected_values = expected_dir_rad;
              case lower('BASIS_LW_SKY')
                  expected_values = expected_lw_sky;
              case lower('BASIS_TS')
                  expected_values = expected_ts;
              case lower('BASIS_PVF')
                  expected_values = expected_pvf;
              otherwise
                  expected_values = nan; % for other inputs
          end
          if size(expected_values,2)>1 && ~any(strcmp(inpValue,expected_values))
               error(['Invalid input ' inpValue ' for ''' inpName '''. '...
                   'Should be one of: ''' strjoin(expected_values, ''' ''') ''''])
          end
          options.(inpName) = inpValue;
       else
          error('%s is not a recognized parameter name',inpName)
       end
    end
    %
    % check consistency:
    if strcmp(options.(lower('BASIS_TS')),'diagnostic')
        if isnan(options.v_10) || isnan(options.bo)
            error('You chose ''diagnostic'' for BASIS_TS. Please provide wind speed (v_10) and Bowen ratio (bo)')
        end
    else
        if isnan(options.(lower('T_w'))) || isnan(options.(lower('T_g')))
            error('You chose ''prescribed'' for BASIS_TS. Please provide wall temperature (T_w) and ground temperature (T_g)')
        end        
    end
    if strcmp(options.(lower('BASIS_DIFF_RAD')),'esra') || ...
            strcmp(options.(lower('BASIS_DIFF_RAD')),'vdi1994')
        if isnan(options.(lower('TLam2')))
            error(['You chose ' options.(lower('BASIS_DIFF_RAD')) ' for BASIS_DIFF_RAD. Please provide Linke turbidity factor TLam2'])
        end         
        if isnan(options.z_nn)
            error(['You chose ' options.(lower('BASIS_DIFF_RAD')) ' for BASIS_DIFF_RAD. Please provide station height z_nn'])
        end           
    end
    if strcmp(options.(lower('BASIS_DIR_RAD')),'esra')  || strcmp(options.(lower('BASIS_DIR_RAD')),'vdi1994')
        if isnan(options.(lower('TLam2')))
            error(['You chose ' options.(lower('BASIS_DIR_RAD')) ' for BASIS_DIR_RAD. Please provide Linke turbidity factor TLam2'])
        end            
        if isnan(options.(lower('z_nn')))
            error(['You chose ' options.(lower('BASIS_DIR_RAD')) ' for BASIS_DIR_RAD. Please provide station height z_nn'])
        end             
    end
    %
    % output
    T_w  = options.(lower('T_w'));
    T_g  = options.(lower('T_g'));
    v_10 = options.(lower('v_10'));
    TLam2= options.(lower('TLam2'));
    bo   = options.(lower('bo'));
    z_nn = options.(lower('z_nn'));
    %
    basis_declin_calc = options.(lower('BASIS_DECLIN_CALC'));
    basis_dir_rad     = options.(lower('BASIS_DIR_RAD'));
    basis_diff_rad    = options.(lower('BASIS_DIFF_RAD'));
    basis_lw_sky      = options.(lower('BASIS_LW_SKY'));
    basis_ts          = options.(lower('BASIS_TS'));
    basis_pvf         = options.(lower('BASIS_PVF'));
    l_dispValues      = options.(lower('l_dispValues'));

end
