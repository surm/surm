function [T_s] = se_calc_Ts(E_swnet, epsilon, ...
                            E_lwin, sigma, T_a2m, bo, v_10, surface_flag)

    % SE_CALC_TW        Calculates wall/ground Temperature with Newton
    %                      approximation assuming immidiate equilibrium
    %
    %  INPUT:
    %
    %    E_swnet             -   SW net radiation balance at surface [Wm^-2]
    %    epsilon             -   emissivity
    %    E_lwnet             -   Lw incoming at surface [Wm^-2]
    %    sigma               -   Stefan-Boltzmann-constant [Wm^-2K^-4]
    %    T_a2m               -   Air Temperature [K]
    %    bo                  -   Bowen ratio
    %    v_10                -   wind speed in 10 m [m/s]
    %    surface_flag        -   Flag: 'w' (wall), 'g' (ground)
    %
    %
    %  OUTPUT:
    %
    %    T_s                 -   Surface temperature [K] 
    %
    %  REFERENCES
    %
    %    Gierisch, A., 2011: Mikroskalige Modellierung meteorologischer und
    %       anthropogener Einflüsse auf die Wä̈rmeabgabe eines Gebäudes
    %       Master’s thesis, Geowissenschaften, University of Hamburg,
    %       Hamburg, Germany, p. 12
    %
    %    Jendritzky, G., G. Menz, W. Schmidt-Kessen, H. Schirmer, 1990:
    %       Methodik zur raumbezogenen Bewertung der thermischen Komponente 
    %       des Bioklima des Menschen (Fortgeschriebenes Klima-Michel-Modell). 
    %       Technical Report 114, Akademie für Raumforschung und 
    %       Landesplanung, Hannover.
    %
    %    Staiger, H., 2014: Die Strahlungskomponente im thermischen 
    %       Wirkungskomplex für operationelle Anwendungen in der Human-
    %       Biometeorologie Ph.D. thesis, Geowissenschaften, 
    %       Albert-Ludwigs-Universit ̈at Freiburg im Breisgau, Germany.
    %
    %% initalize
    T_s = T_a2m;
    %
    %% Newton approximation
    if strcmp(surface_flag, 'w')
        % PARAMETERS              
        % heat transfer between interior and outside
        U    = 1;
        C    = 1/(1/U-0.04);
        % indoor room temperature
        T_in = 20+273.15;
        % heat transfer coefficient
        h_aw = 4 + 4 * v_10; % wall
        % CALCULATION                        
        for i = 1:3 % three iteration steps enough according to Staiger (2014)
            T_s = T_s - ...
                    ((E_swnet + ...
                      epsilon * (E_lwin - sigma * T_s^4)) ...
                     - C * (T_in - T_s) ...
                     + h_aw * (T_a2m - T_s))...
                  /...
                  (-4 * epsilon * sigma * T_s^3 ...
                   + C - h_aw);
        end
    else
        % PARAMETERS              
        %  heat transfer coefficient
        h_ag = 6.2 + 4.26 * v_10; % ground
        % parameter of soil characteristics
        if E_swnet > 0 %during the day
            b_terr = -0.19;
        else
            b_terr = -0.32;
        end        
        % CALCULATION                    
        for i = 1:3 % three iteration steps enough according to Staiger (2014)
            T_s = T_s - ...
                     ((E_swnet ...
                      + epsilon * (E_lwin - sigma * T_s^4))...
                       * (1 + b_terr) + ...
                      + (1 + 1/bo) * h_ag * (T_a2m - T_s))...
                     /...
                     (-4 * epsilon * sigma * (1 + b_terr) * T_s^3 ...
                      - (1 + 1/bo) * h_ag);
        end
    end
end
