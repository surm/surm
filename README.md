 SURM
=============== 
The Simple Urban Radiation Model (SURM) is a model to calculate mean radiant temperature (Tmrt) in an idealised symmetric street canyon. 

The model can be used either stand-alone with standard meteorological parameters as inputs or for radiation modifications in a built-up area with radiation fluxes from a mesoscale model or measurements.

The model is written in Matlab but it can also be used in the freely available GNU Octave environment (www.gnu.org/software/octave/) with the mapping packages (https://octave.sourceforge.io/mapping/index.html).

## Getting started

The program `model/get_started.m` provides an example, which variables have to be set for SURM and how to call the main routine.

`tests/plausibility_checks.m` provides another example for the plausibility tests described in Section 3.2 in Fischereit (2020).

Detailed description of variables and parameterisations can be found in Fischereit (2020) and in `model/calcTmrt`.

Example Linke turbidity factors of air mass 2 for Hamburg are located in `model/tlam2.txt`. For other locations those need to be adjusted as described in Fischereit (2020).

## Reference

Fischereit, J. (2020): The Simple Urban Radiation Model for estimating Mean Radiant Temperature in idealized street canyons, Urban Climate, https://doi.org/10.​1016/​j.​uclim.​2020.​100694
